A simple module to allow the GM to lock the movement of a token.

I ended up creating it because I was using token attacher to essentially create prefabricated scene pieces and kept pulling them around the map by accident.

It is small enough that it shouldn't impact on anything else, but I take no responsibility if it accidentally sells your Grandmother or something.
