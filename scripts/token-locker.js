class TokenLocker{
    static async addLockButton(app, html, data){

        if(!data.isGM) {return;}

        //add a lock button to token HUD
        function enableButton(tbutton){

            //attach clcik handler if not bound
            if(!tbutton.hasClass('clickBound'))
            {
                tbutton.click(async (ev) => onButtonClick(ev, tbutton));
                tbutton.addClass('clickBound');
            }
        }
        

        let statusLock = app.object.document.getFlag("token-locker", "lockStatus");
        if (statusLock == undefined || statusLock == null) {
            statusLock = false;
            await app.object.document.setFlag("token-locker", "lockStatus", false)
        }
        
        // define button
        let lockButton = $(`<div class="control-icon locker" title="Lock Token Movement"><i class="fas fa-lock"></i></div>`);



        if(data.isGM)
            enableButton(lockButton);

        async function onButtonClick(ev, tbutton)
        {
            if(tbutton != lockButton) return;

            ev.preventDefault();
            ev.stopPropagation();


            //console.log("clicked on a button")

            statusLock = !statusLock;
            await app.object.document.setFlag("token-locker", "lockStatus", statusLock);

        };

        

        if (statusLock){
            lockButton.addClass('active');
        }
        else{
            lockButton.removeClass('active');

        }        

        //insert button into column
        html.find('.col.right').prepend(lockButton);
    };


}

Hooks.on('ready', () => {
    Hooks.on('renderTokenHUD', (app, html, data) => {TokenLocker.addLockButton(app, html, data)});
});

Hooks.on('preUpdateToken', (document, changes, options) =>{
    if((changes.x != null || changes.y != null) && document.getFlag('token-locker', 'lockStatus')){
        ui.notifications.info("Trying to move locked token");
        return false;
    }
    
});

